<span class="<?php print $wheels_size[1]; ?>">

<?php // Image.
    $img = theme('image', $wheel['Image'], $alt = '', $title = '', $attributes = array('width' => '100', 'height' => '100'), $getsize = FALSE);
    ?>
    
    <div class="img-list"> 
        <?php print l($img, 'node/' . $node->nid, array('attributes' => array('target' => '_blank'), 'fragment' => 'refresh', 'html' => TRUE)); ?> 
    </div>

    
<?php // Title.
    $title_text = truncate_utf8($node->title, 22, $wordsafe = FALSE, $dots = TRUE);
    ?>
    <div class="title-list"> <?php print l($title_text, 'node/' . $node->nid, array('attributes' => array('target' => '_blank'))) ?> </div>

    
<?php // Car image. ?>
    <div class="car-img-list">
        <a href="<?php print $wheel['ImageOnCarBinary'] ?>" rel="shadowbox" player="img" > 
            <?php print t('See on your Car'); ?>
        </a>
    </div>

<?php // Category.  ?>
    <div class="tag-list">
        <a>
            <div class="taxonomy">
                <?php print $wheel['Offset']; ?>
            </div>
        </a> 
        <a>
            <div class="taxonomy">
                <?php print $wheel['BoltPattern'] . '/' . $wheel['BoltCircle']; ?>
            </div>
        </a>
        <a>
            <div class="taxonomy">
            <?php print $wheel['Dimension']; ?>
            </div>
        </a>
        <a>
            <div class="taxonomy">     
            <?php print $wheel['ColourText']; ?>
            </div>
        </a>

    </div>

<?php // Price. ?>
    <div class="price-list">
        <?php print uc_price($node->sell_price); ?> 
    </div>


<?php // Add to cart. ?>
    <div class="buy-list">
        <?php print drupal_get_form('uc_catalog_buy_it_now_form_' . $node->nid, $node); ?>
    </div>


</span>