<?php

/**
 * @file
 * File with context
 */

/**
 * Implements of hook_context_default_contexts().
 */
function alcar_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'configurator';
  $context->description = 'configurator';
  $context->tag = 'Configurator';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'configurator' => 'configurator',
        'configurator*' => 'configurator*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'alcar-0' => array(
          'module' => 'alcar',
          'delta' => 0,
          'region' => 'content',
          'weight' => -10,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Configurator');
  t('configurator');
  $export['configurator'] = $context;

  return $export;
}
