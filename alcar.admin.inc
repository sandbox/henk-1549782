<?php

/**
 * @file
 * Administration page callbacks for the alcar module.
 */

/**
 * Form builder. Configure ALCAR aplication.
 * @ingroup forms system_settings_form().
 */
function alcar_admin_settings() {

  $form['alcar_user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#default_value' => variable_get('alcar_user', 'yourname'),
    '#description' => t('Insert your ALCAR user name .'),
  );

  $form['alcar_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('alcar_password', 'yourpassword'),
    '#description' => t('Insert your ALCAR password.'),
  );

  $form['alcar_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => variable_get('alcar_host', 'test.aez-wheels.com'),
    '#description' => t('Insert your ALCAR host.'),
  );

  $form['alcar_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => variable_get('alcar_path', '/'),
    '#description' => t('Insert your ALCAR path.'),
  );

  $form['alcar_sheme'] = array(
    '#type' => 'select',
    '#title' => t('HTTP/HTTPS'),
    '#options' => array(
      'https' => 'HTTPS',
      'http' => 'HTTP',
    ),
    '#default_value' => variable_get('alcar_sheme', 'https'),
    '#description' => t('Insert your ALCAR sheme.'),
  );

  $form['alcar_code'] = array(
    '#type' => 'textfield',
    '#title' => t('PartyID'),
    '#default_value' => variable_get('alcar_code', 'QH9999999'),
    '#description' => t('Insert your ALCAR code.'),
  );


  return system_settings_form($form);
}
