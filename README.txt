Alcar 6.x.2.x-dev  updated 2012-11-24
=======================

This module integrate ALCAR Adhoc-EDI Version 2.7 API with Drupal 6 and Ubercart 2.
It is an open source project, licensed under GNU AGPL 3, and
coordinated by Ratio Web.

This module provide list of compar products and block with car select forms. 
Users can select car and see all connected with this car products.


Installation
============

1. Download from drupal.org to Your sites/all/modules folder
2. Enable the module.



Configuration
=====================================
1. Admin makes a permission available that allows to "Administer Alcar"
   to permissioned users. 
   Users with the "Use Alcar" permission will be able to use the Alcar module.

2. Contact with Alcar http://www.alcar.de/4339_DE.htm and get ID and key

3. Set up your settings `admin/settings/alcar`.



Contributors
============
- henk (Karol Bryksa)

